<?php

namespace App\Http\Controllers;

use App\Conductor;
use Illuminate\Http\Request;

class ConductorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $conductors = Conductor::get();
        echo json_encode($conductors);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $conductor = new Conductor();
        $conductor->numero_cedula = $request->input('numero_cedula');
        $conductor->primer_nombre = $request->input('primer_nombre');
        $conductor->segundo_nombre = $request->input('segundo_nombre');
        $conductor->apellidos = $request->input('apellidos');
        $conductor->direccion = $request->input('direccion');
        $conductor->telefono = $request->input('telefono');
        $conductor->ciudad = $request->input('ciudad');
        $conductor->save();
        echo json_encode($conductor);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Conductor  $conductor_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $conductor_id)
    {
        $conductor = Conductor::find($conductor_id);
        $conductor->numero_cedula = $request->input('numero_cedula');
        $conductor->primer_nombre = $request->input('primer_nombre');
        $conductor->segundo_nombre = $request->input('segundo_nombre');
        $conductor->apellidos = $request->input('apellidos');
        $conductor->direccion = $request->input('direccion');
        $conductor->telefono = $request->input('telefono');
        $conductor->ciudad = $request->input('ciudad');
        $conductor->save();
        echo json_encode($conductor);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Conductor  $conductor_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($conductor_id)
    {
        $conductor = Conductor::find($conductor_id);
        $conductor->delete();
    }
}
