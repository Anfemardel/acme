<?php

namespace App\Http\Controllers;

use App\Persona;
use Illuminate\Http\Request;

class PersonaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $personas = Persona::get();
        echo json_encode($personas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $persona = new Persona();
        $persona->numero_cedula = $request->input('numero_cedula');
        $persona->primer_nombre = $request->input('primer_nombre');
        $persona->segundo_nombre = $request->input('segundo_nombre');
        $persona->apellidos = $request->input('apellidos');
        $persona->direccion = $request->input('direccion');
        $persona->telefono = $request->input('telefono');
        $persona->ciudad = $request->input('ciudad');
        $persona->save();
        echo json_encode($persona);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Persona  $persona_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Persona $persona_id)
    {
        $persona = Persona::find($persona_id);
        $persona->numero_cedula = $request->input('numero_cedula');
        $persona->primer_nombre = $request->input('primer_nombre');
        $persona->segundo_nombre = $request->input('segundo_nombre');
        $persona->apellidos = $request->input('apellidos');
        $persona->direccion = $request->input('direccion');
        $persona->telefono = $request->input('telefono');
        $persona->ciudad = $request->input('ciudad');
        $persona->save();
        echo json_encode($persona);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Persona  $persona
     * @return \Illuminate\Http\Response
     */
    public function destroy($persona_id)
    {
        $persona = Persona::find($persona_id);
        $persona->delete();
    }
}
