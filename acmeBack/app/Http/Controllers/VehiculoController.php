<?php

namespace App\Http\Controllers;

use App\Vehiculo;
use App\Persona;
use App\Conductor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class VehiculoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehiculos = Vehiculo::get()->toArray();
        for($i=0; $i < count($vehiculos);$i++){
            $persona = Persona::where("id", $vehiculos[$i]["propietario_id"])->get()->toArray();
            $conductor = Conductor::where("id", $vehiculos[$i]["conductor_id"])->get()->toArray();
            $vehiculos[$i]["propietario"] = $persona[0]["primer_nombre"]." ".$persona[0]["apellidos"];
            $vehiculos[$i]["conductor"] = $conductor[0]["primer_nombre"]." ".$conductor[0]["apellidos"];
        }
        echo json_encode($vehiculos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $vehiculo = new Vehiculo();
        $vehiculo->placa = $request->input('placa');
        $vehiculo->color = $request->input('color');
        $vehiculo->marca = $request->input('marca');
        $vehiculo->tipo_vehiculo = $request->input('tipo_vehiculo');
        $vehiculo->conductor_id = $request->input('conductor_id');
        $vehiculo->propietario_id = $request->input('propietario_id');
        $vehiculo->save();
        echo json_encode($vehiculo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vehiculo  $vehiculo_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $vehiculo_id)
    {
        $vehiculo = Vehiculo::find($vehiculo_id);
        $vehiculo->placa = $request->input('placa');
        $vehiculo->color = $request->input('color');
        $vehiculo->marca = $request->input('marca');
        $vehiculo->tipo_vehiculo = $request->input('tipo_vehiculo');
        $vehiculo->conductor_id = $request->input('conductor_id');
        $vehiculo->propietario_id = $request->input('propietario_id');
        $vehiculo->save();
        echo json_encode($vehiculo);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vehiculo  $vehiculo_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($vehiculo_id)
    {
        $vehiculo = Vehiculo::find($vehiculo_id);
        $vehiculo->delete();
    }
}
