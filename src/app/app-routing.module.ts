import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { VehiculoComponent } from './vehiculo/vehiculo.component';
import { ConductorComponent } from './conductor/conductor.component';
import { HomeComponent } from './home/home.component';
import { PersonaComponent } from './persona/persona.component';


const routes: Route[] =[
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'vehiculo', component: VehiculoComponent },
  { path: 'persona', component: PersonaComponent },
  { path: 'conductor', component: ConductorComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
