import { Component, OnInit } from '@angular/core';
import { ConductorsService } from '../services/conductors.service';
import { Conductor } from '../interfaces/conductor';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-conductor',
  templateUrl: './conductor.component.html',
  styleUrls: ['./conductor.component.css']
})
export class ConductorComponent implements OnInit {
  conductor: Conductor = {
    numero_cedula: null,
    primer_nombre: null,
    segundo_nombre: null,
    apellidos: null,
    direccion: null,
    telefono: null,
    ciudad: null
  };
  conductors: Conductor[];
  id: any;
  editing: boolean = false;

  constructor(private conductorsService: ConductorsService,
    private readonly spinner: NgxSpinnerService) {

  }

  ngOnInit() {
    this.getConductor();
  }
  getConductor() {
    this.spinner.show();
    this.conductorsService.get().subscribe((data: Conductor[]) => {
      this.conductors = data;
      this.spinner.hide();
    }, (error) => {
      console.log(error);
      alert('A ocurrido un error');
      this.spinner.hide();
    });
  }
  saveConductor() {
    if (this.editing) {
      this.conductorsService.put(this.conductor).subscribe((data) => {
        alert('Usuario Actualizado');
      }, (error) => {
        console.log(error);
        alert('Ocurrio Error');
      });
    } else {
      this.conductorsService.save(this.conductor).subscribe((data) => {
        alert('Usuario Guardado');
        this.getConductor();
      }, (error) => {
        console.log(error);
        alert('Ocurrio Error');
      });
    }
  }
  delete(id) {
    this.conductorsService.delete(id).subscribe((data) => {
      alert('eliminado con exito!');
    }, (error) => {
      console.log(error);
    });
  }
}
