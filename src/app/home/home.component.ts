import { Component, OnInit } from '@angular/core';
import { VehiculosService } from '../services/vehiculos.service'
import { Vehiculo } from '../interfaces/vehiculo'
import { Persona } from '../interfaces/persona';
import { PersonasService } from '../services/personas.service';
import { ConductorsService } from '../services/conductors.service';
import { Conductor } from '../interfaces/conductor';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  vehiculos: Vehiculo[];
  personas: Persona[];
  conductors: Conductor[];
  constructor(private vehiculosService: VehiculosService,
    private personasService: PersonasService,
    private conductorsService: ConductorsService,
    private readonly spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.getVehiculos();
    this.getConductor();
    this.getPersona();
  }

  getConductor() {
    this.conductorsService.get().subscribe((data: Conductor[]) => {
      this.conductors = data;
    }, (error) => {
      console.log(error);
      alert('A ocurrido un error');
    });
  }

  getPersona() {
    this.personasService.get().subscribe((data: Persona[]) => {
      this.personas = data;
    }, (error) => {
      console.log(error);
      alert('A ocurrido un error');
    });
  }

  getVehiculos() {
    this.spinner.show();
    this.vehiculosService.get().subscribe((data: Vehiculo[]) => {
      this.vehiculos = data;
      this.spinner.hide();
    }, (error) => {
      console.log(error);
      alert('A ocurrido un error');
      this.spinner.hide();
    });
  }
  delete(id) {
    this.vehiculosService.delete(id).subscribe((data) => {
      alert('eliminado con exito!');
    }, (error) => {
      console.log(error);
    });
  }

}
