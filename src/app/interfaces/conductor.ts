export interface Conductor {
    id?: number;
    numero_cedula: number;
    primer_nombre: string;
    segundo_nombre: string;
    apellidos: string;
    direccion: string;
    telefono: number;
    ciudad: string;
    created_at?: string;
    update_at?: string;
}