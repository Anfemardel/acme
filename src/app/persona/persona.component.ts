import { Component, OnInit } from '@angular/core';
import { Persona } from '../interfaces/persona';
import { PersonasService } from '../services/personas.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.css']
})
export class PersonaComponent implements OnInit {
  personas: Persona[];
  persona: Persona = {
    numero_cedula: null,
    primer_nombre: null,
    segundo_nombre: null,
    apellidos: null,
    direccion: null,
    telefono: null,
    ciudad: null
  };
  id: any;
  editing: boolean = false;
  constructor(private personasService: PersonasService,
    private readonly spinner: NgxSpinnerService) {

  }

  ngOnInit() {
    this.getPersona();
  }
  getPersona() {
    this.spinner.show();
    this.personasService.get().subscribe((data: Persona[]) => {
      this.personas = data;
      this.spinner.hide();
    }, (error) => {
      console.log(error);
      alert('A ocurrido un error');
      this.spinner.hide();
    });
  }
  savePersona() {
    if (this.editing) {
      this.personasService.put(this.persona).subscribe((data) => {
        alert('Usuario Actualizado');
      }, (error) => {
        console.log(error);
        alert('Ocurrio Error');
      });
    } else {
      this.personasService.save(this.persona).subscribe((data) => {
        alert('Usuario Guardado');
        this.getPersona();
      }, (error) => {
        console.log(error);
        alert('Ocurrio Error');
      });
    }
  }
  delete(id) {
    this.personasService.delete(id).subscribe((data) => {
      alert('eliminado con exito!');
    }, (error) => {
      console.log(error);
    });
  }

}
