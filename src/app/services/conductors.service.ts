import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Conductor } from '../interfaces/conductor'

@Injectable({
  providedIn: 'root'
})
export class ConductorsService {
  API_ENDPOINT = 'http://localhost:8000/api';
  constructor(private httpClient: HttpClient) { }
  //GET
  get(){
    return this.httpClient.get(this.API_ENDPOINT+'/conductors');
  }
  //POST
  save(conductor: Conductor){
    const headers = new HttpHeaders({'Content-Type':'aplication/json'});
    return this.httpClient.post(this.API_ENDPOINT + '/conductors', conductor,{headers: headers});
  }
  //PUT
  put(conductor){
    const headers = new HttpHeaders({'Content-Type':'aplication/json'});
    return this.httpClient.put(this.API_ENDPOINT + '/conductors/'+conductor.id, conductor,{headers: headers});
  }
  //DELETE
  delete(id){
    return this.httpClient.delete(this.API_ENDPOINT+'/conductors/'+id);
  }
}
