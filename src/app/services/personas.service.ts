import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Persona } from '../interfaces/persona';

@Injectable({
  providedIn: 'root'
})
export class PersonasService {
  API_ENDPOINT = 'http://localhost:8000/api';
  constructor(private httpClient: HttpClient) { }
  //GET
  get(){
    return this.httpClient.get(this.API_ENDPOINT+'/personas');
  }
  //POST
  save(persona: Persona){
    const headers = new HttpHeaders({'Content-Type':'aplication/json'});
    return this.httpClient.post(this.API_ENDPOINT + '/personas', persona,{headers: headers});
  }
  //PUT
  put(persona){
    const headers = new HttpHeaders({'Content-Type':'aplication/json'});
    return this.httpClient.put(this.API_ENDPOINT + '/personas/'+persona.id, persona,{headers: headers});
  }
  //DELETE
  delete(id){
    return this.httpClient.delete(this.API_ENDPOINT+'/personas/'+id);
  }
}
