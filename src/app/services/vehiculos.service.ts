import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Vehiculo } from '../interfaces/vehiculo';

@Injectable({
  providedIn: 'root'
})
export class VehiculosService {

  API_ENDPOINT = 'http://localhost:8000/api';
  constructor(private httpClient: HttpClient) { }
  //GET
  get(){
    return this.httpClient.get(this.API_ENDPOINT+'/vehiculos');
  }
  //POST
  save(vehiculo: Vehiculo){
    const headers = new HttpHeaders({'Content-Type':'aplication/json'});
    return this.httpClient.post(this.API_ENDPOINT + '/vehiculos', vehiculo,{headers: headers});
  }
  //PUT
  put(vehiculo){
    const headers = new HttpHeaders({'Content-Type':'aplication/json'});
    return this.httpClient.put(this.API_ENDPOINT + '/vehiculos/'+vehiculo.id, vehiculo,{headers: headers});
  }
  //DELETE
  delete(id){
    return this.httpClient.delete(this.API_ENDPOINT+'/vehiculos/'+id);
  }
}
