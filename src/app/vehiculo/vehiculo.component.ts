import { Component, OnInit } from '@angular/core';
import { Vehiculo } from '../interfaces/vehiculo';
import { VehiculosService } from '../services/vehiculos.service';
import { ActivatedRoute } from '@angular/router';
import { Persona } from '../interfaces/persona';
import { PersonasService } from '../services/personas.service';

import { ConductorsService } from '../services/conductors.service';
import { Conductor } from '../interfaces/conductor';

@Component({
  selector: 'app-vehiculo',
  templateUrl: './vehiculo.component.html',
  styleUrls: ['./vehiculo.component.css']
})
export class VehiculoComponent implements OnInit {
  vehiculo: Vehiculo ={
    placa: null,
    color: null,
    marca: null,
    tipo_vehiculo: null,
    conductor_id: null,
    propietario_id: null
  };
  id: any;
  editing: boolean = false;
  vehiculos: Vehiculo[];
  personas: Persona[];
  conductors: Conductor[];
  constructor(private vehiculosService: VehiculosService,
    private activatedRoute: ActivatedRoute,
    private personasService: PersonasService,
    private conductorsService: ConductorsService) { }

  ngOnInit() {
    this.getPersona();
    this.getConductor();
  }

  getPersona(){
    this.personasService.get().subscribe((data: Persona[])=>{
      this.personas=data;
    },(error)=>{
      console.log(error);
      alert('A ocurrido un error');
    });
  }

  getConductor(){
    this.conductorsService.get().subscribe((data: Conductor[])=>{
      this.conductors=data;
    },(error)=>{
      console.log(error);
      alert('A ocurrido un error');
    });
  }

  saveVehiculo(){
    if(this.editing){
      this.vehiculosService.put(this.vehiculo).subscribe((data)=>{
        alert('Usuario Actualizado');
      },(error)=>{
        console.log(error);
        alert('Ocurrio Error');
      });
    }else{
      this.vehiculosService.save(this.vehiculo).subscribe((data)=>{
        alert('Vehiculo Guardado');
      },(error)=>{
        console.log(error);
        alert('Ocurrio Error');
      });
    }
  }

}
